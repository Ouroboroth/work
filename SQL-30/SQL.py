import sqlite3

con = sqlite3.connect("workers.db")
c = con.cursor()

c.execute('DROP TABLE IF EXISTS Workers')
con.commit()

c.execute("""
CREATE TABLE Workers (
   id int auto_increment primary key,
   name varchar(100),
   age int,
   salary int,
   login varchar(100),
   description varchar(100),
   dt TIMESTAMP
   DEFAULT CURRENT_TIMESTAMP
)
""")

values = [
   (1, "Дима", 23, 400, "aaa", "Some sample text right here"),
   (2, "Петя", 25, 500, "eee", "Some sample text right here1"),
   (3, "Вася", 23, 500, "kkk", "Some sample text right here2"),
   (4, "Коля", 30, 1000, "eka", "Some sample text right here3"),
   (5, "Иван", 27, 500, "yyy", "Some sample text right here4"),
   (6, "Кирилл", 28, 1000, "kak", "Some sample text right here5"),
]

c.executemany('INSERT INTO Workers (id, name, age, salary, login, description) VALUES (?, ?, ?, ?, ?, ?)', values)
con.commit()

c.execute("SELECT * FROM Workers")
con.commit()
print(c.fetchall())

print(' ')
print('34')
# Задача 34
c.execute('SELECT *,  datetime(dt, "+1 day") FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('35')
# Задача 35
c.execute('SELECT *, datetime(dt, "-1 day") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('36')
# Задача 36
c.execute('SELECT *, datetime(dt, "+1 day", "+2 hour") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('37')
# Задача 37
c.execute('SELECT *, datetime(dt, "+1 year", "+2 month") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('38')
# Задача 38
c.execute('SELECT *, datetime(dt, "+1 day", "+2 hour", "+3 minute") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('39')
# Задача 39
c.execute('SELECT *, datetime(dt, "+1 day", "+2 hour", "+3 minute", "+5 second") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('40')
# Задача 40
c.execute('SELECT *, datetime(dt, "+2 hour", "+3 minute", "+5 second") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('41')
# Задача 41
c.execute('SELECT *, datetime(dt, "+1 day", "-2 hour") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('42')
# Задача 42
c.execute('SELECT *, datetime(dt, "+1 day", "-2 hour", "-3 minute") AS date1 FROM Workers')
con.commit()
print(c.fetchall())

print(' ')
print('43')
# Задача 43
c.execute('SELECT *, 3 AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('44')
# Задача 44
c.execute('SELECT *, \'eee\' AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('45')
# Задача 45
c.execute('SELECT *, 3 AS \'3\' FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('46')
# Задача 46
c.execute('SELECT *, (salary + age) AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('47')
# Задача 47
c.execute('SELECT *, (salary - age) AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('48')
# Задача 48
c.execute('SELECT *, (salary * age) AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('49')
# Задача 49
c.execute('SELECT *, (salary + age) / 2 AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('50')
# Задача 50
c.execute('SELECT * FROM workers WHERE (CAST(strftime(\'%d\', dt) AS INTEGER) + CAST(strftime(\'%m\', dt) AS INTEGER)) < 10')
con.commit()
print(c.fetchall())

print(' ')
print('51')
# Задача 51
c.execute('SELECT SUBSTR(description, 0, 6) AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('52')
# Задача 52
c.execute('SELECT SUBSTR(description, -1, -6) AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('53')
# Задача 53
c.execute('SELECT SUBSTR(description, 2, 8) AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('54')
# Задача 54
# c.execute('SELECT id, name FROM category UNION SELECT id, name FROM sub_category')
con.commit()
print(c.fetchall())

print(' ')
print('55')
# Задача 55
c.execute('SELECT *, salary || age AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('56')
# Задача 56
c.execute('SELECT *, salary || age || "!!!" AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('57')
# Задача 57
c.execute('SELECT *, salary || "-" || age AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('58')
# Задача 58
c.execute('SELECT *, SUBSTR(login, 0, 5) || "..." AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('59')
# Задача 59
c.execute('SELECT age, MIN(salary) FROM workers GROUP BY age')
con.commit()
print(c.fetchall())

print(' ')
print('60')
# Задача 60
c.execute('SELECT salary, MAX(age) FROM workers GROUP BY salary')
con.commit()
print(c.fetchall())

print(' ')
print('61')
# Задача 61
c.execute('SELECT DISTINCT age, group_concat(id, "-") AS res FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('62')
# Задача 62
c.execute('SELECT * FROM workers WHERE salary > (SELECT AVG(salary) FROM workers)')
con.commit()
print(c.fetchall())

print(' ')
print('63')
# Задача 63
c.execute('SELECT * FROM workers WHERE age < ((SELECT AVG(age) FROM workers) / 2 *3 )')
con.commit()
print(c.fetchall())

print(' ')
print('64')
# Задача 64
c.execute('SELECT * FROM workers WHERE salary = (SELECT MIN(salary) FROM workers)')
con.commit()
print(c.fetchall())

print(' ')
print('65')
# Задача 65
c.execute('SELECT * FROM workers WHERE salary = (SELECT MAX(salary) FROM workers)')
con.commit()
print(c.fetchall())

print(' ')
print('66')
# Задача 66
c.execute('SELECT MAX(salary) AS max_salary FROM workers WHERE age = 25')
con.commit()
print(c.fetchall())

print(' ')
print('67')
# Задача 67
c.execute('SELECT ((SELECT MAX(age) FROM workers) - (SELECT MIN(age) FROM workers)) / 2 AS avg FROM workers')
con.commit()
print(c.fetchall())

print(' ')
print('68')
# Задача 68
c.execute('SELECT ((SELECT MAX(salary) FROM workers WHERE age = 25) - (SELECT MIN(salary) FROM workers WHERE age = 25)) / 2 AS avg FROM workers')
con.commit()
print(c.fetchall())

c.close()
con.close()
