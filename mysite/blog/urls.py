from django.conf.urls import url

from . import views

app_name = 'blog'
urlpatterns = [
    url(r'^$', views.home, name='home'), # пустой адрес соответствует странице home
	url(r'^blog/$', views.blog, name='blog'),
    url(r'^blog/(?P<post_id>[0-9]+)/$', views.post, name='post') # адрес с цифрой соответствует посту
]