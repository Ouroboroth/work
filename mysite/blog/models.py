from django.db import models
from django.utils import timezone

class Post(models.Model): # модель поста с автором, датой, названием и текстом
    author = models.ForeignKey('auth.User')
    pub_date = models.DateTimeField( default=timezone.now )
    title = models.CharField(max_length=200)
    text = models.TextField()

    def publish(self):
        self.pub_date = timezone.now()
        self.save()
        
    def __str__(self):
        return self.title
        
class Comment(models.Model): #Модель комментария с постом, к которому он идет, текстом и датой
    rel_post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    pub_date = models.DateTimeField( default=timezone.now )
    
    def publish(self):
        self.pub_date = timezone.now()
        self.save()
        
    def __str__(self):
        return self.rel_post.title + ' ' + self.text