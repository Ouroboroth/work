from django.contrib import admin
from .models import Post, Comment
# из админки
admin.site.register(Post) # можно написать пост
admin.site.register(Comment) # и комментарий к нему
