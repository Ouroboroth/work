from django.shortcuts import get_object_or_404,render
from .models import Post, Comment
from .forms import CommentForm

def home(request): # Рендеринг домашней страницы
    return render(request, 'pages/home.html')
    
def blog(request):
    posts = Post.objects.order_by('-pub_date')  # рендеринг страницы с постами
    return render(request, 'pages/posts.html', {'posts': posts} )
 
def post(request, post_id): # рендеринг страницы с каждым отдельным постом
    post = get_object_or_404(Post, pk=post_id)
    comments = Comment.objects.filter(rel_post = post_id) # и комментариями, которые пока что можно писать только из админки
    form = CommentForm() 
    return render(request, 'pages/post_page.html', {'post': post, 'comments': comments, 'form': form})