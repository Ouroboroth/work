import sqlite3
import datetime

# Подключение к базе
con = sqlite3.connect("workers.db")

# Создание курсора
c = con.cursor()

# Создание таблиц
c.execute("""
DROP TABLE IF EXISTS Department
""")
c.execute("""
CREATE TABLE Department (
   id int auto_increment primary key,
   name varchar(100)
)
""")

c.execute("""
DROP TABLE IF EXISTS Employee
""")
c.execute("""
CREATE TABLE Employee (
   id int auto_increment primary key,
   department_id int,
   chief_id int,
   name varchar(100),
   salary int,
   FOREIGN KEY (department_id) REFERENCES Department(id),
   FOREIGN KEY (chief_id) REFERENCES Employee(id)
)
""")


# Наполнение таблицы
values = [(1, 'робототехники'), (2, 'шаурмичный'), (3, 'котиков'), (4, 'счастья')]
c.executemany("""INSERT INTO Department (id, name) VALUES (?, ?)""", values)


values = [
    (1, 1, 1, "Петя", 200),
    (2, 2, 1, "Валя", 100),
    (3, 4, 1, "Аня", 400),
    (4, 3, 1, "Соня", 500),
    (5, 2, 2, "Рита", 500),
    (6, 2, 2, "Кеша", 200),
    (7, 3, 4, "Гена", 300),
    (8, 2, 4, "Варя", 900)
]
c.executemany("""INSERT INTO Employee (id, department_id, chief_id, name, salary) VALUES (?, ?, ?, ?, ?)""", values)
# Подтверждение отправки данных в базу
con.commit()

# 5 сложных задач

# Задача 1
c.execute("""
SELECT * FROM Employee
WHERE id IN
(SELECT id FROM
(SELECT e1.id, e1.salary as w_s, e2.salary as c_s FROM Employee as e1
JOIN
Employee as e2
ON e1.chief_id = e2.id) as e3
where e3.w_s > e3.c_s)
""")
print(c.fetchall())

# Задача 2
c.execute("""
SELECT * FROM (
SELECT * FROM Employee
ORDER BY salary ASC
)
GROUP BY department_id
""")
print(c.fetchall())

# Задача 3
c.execute("""
SELECT department_id FROM Employee
GROUP BY department_id
HAVING COUNT(department_id) <= 3
""")
print(c.fetchall())

# Задача 4
c.execute("""
SELECT * FROM Employee
WHERE id IN
(SELECT id FROM 
(SELECT e1.id, e1.department_id as my_dep, e1.chief_id, e2.id, e2.department_id as chf_dep FROM Employee as e1
JOIN
Employee as e2
ON e1.chief_id = e2.id) as e3
WHERE e3.my_dep != e3.chf_dep)
""")
print(c.fetchall())

# Задача 5 (у меня не получилась)


# Завершение соединения
c.close()
con.close()
