# -*- coding: utf-8 -*-

import thread
import socket
import time
import sqlite3
import sys
reload(sys)
sys.setdefaultencoding('utf8')


def main():
    s = socket.socket()
    ip = socket.gethostname()
    port = 666
    s.bind((ip, port))  # Создание сервера

    global clientlist
    clientlist = []  # Обновляемый список клиентов

    global readbuf
    readbuf = ""  # Буфер сообщений

    def broadcast():  # Функция отправки сообщений из буфера сообщений всем клиентам и запись их в БД
        hist = sqlite3.connect("Chat logs.db")
        curs = hist.cursor()
        curs.execute("""
            CREATE TABLE IF NOT EXISTS ChatHist (
            time TIMESTAMP,
            user VARCHAR(512),
            message VARCHAR(4096)
            )
            """)

        global readbuf
        global clientlist

        while True:
            if readbuf != "":
                temp = readbuf.split("\n")
                readbuf = temp.pop()
                for line in temp:
                    print time.strftime("%c") + " " + line
                    uzer = line[:line.find(":")]
                    mess = line[line.find(": "):]
                    valline = time.strftime("%c"), uzer, mess
                    curs.execute("""INSERT INTO ChatHist (time, user, message) VALUES (?, ?, ?)""", valline)
                    hist.commit()
                    for client in clientlist:
                        try:
                            client[0].send(time.strftime("%c") + " " + line)
                        except socket.error:
                            clientlist.remove(client)
                            client[0].close()

    def new_client(cl, addr, nick):  # Прием сообщений от клиента
        global readbuf
        print ("Client {} connected from {}".format(nick, addr))
        while True:
            try:
                while True:
                    readbuf += nick + ": " + cl.recv(4096) + "\n"
                    time.sleep(1)
            except socket.error:
                break
        clientlist.remove((cl, nick))
        cl.close()
        print "User {} disconnected from your channel".format(nick)

    thread.start_new_thread(broadcast, ())  # Запуск потока с функцией отправки и записи сообщений

    while True:
        s.listen(1)
        c, adr = s.accept()
        nickname = c.recv(512)
        time.sleep(0.1)
        clientlist.append((c, nickname))
        thread.start_new_thread(new_client, (c, adr, nickname))
        time.sleep(1)


if __name__ == '__main__':
    main()