# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import socket
import thread
import time

def readchat(s, msg):
    while True:
        try:
            msg = s.recv(4096).decode("utf-8")
            print msg
        except socket.error:
            break
    print "Connection closed"
    return 0

def main():
    s = socket.socket()
    host = socket.gethostname()
    port = 666

    nick = raw_input("Enter your nickname: ")
    try:
        s.connect((host, port))
    except socket.error:
        print "No such server"
        return 0
    msg = ""
    s.send(nick)
    thread.start_new_thread(readchat, (s, msg))

    while True:
        text = raw_input()
        s.send(text)

    s.close()

if __name__ == '__main__':
    main()